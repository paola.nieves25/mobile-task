import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image
} from "react-native";

import { Card, CardItem, Thumbnail, Body, Left, Right, Button, Icon,Input, Label } from 'native-base'
import ProfileTab from "./AppTabNavigator/ProfileTab.js";





var djson = require('../server/data.json');
const photo = djson.search[0]; 
class CardComponent extends Component {


      

    constructor(props) {
        super(props);
    
        this.state = {
          data: null,
          
        };
      }
      



    


    render() {
        

        const images = {

            "1":  djson.posts[1].image ,
            "2": djson.posts[0].image ,
        
            
        }



        return (
            
            <Card>
                <CardItem>
                    <Left>
                        <Thumbnail source={{ uri: this.props.imageP}} />
                        <Body>
                      <Text>{this.props.usern}</Text>
                            <Text note>{this.props.dateC}</Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem cardBody>
                    <Image source={{ uri:images[this.props.imageSource]}} style={{ height: 200, width: null, flex: 1 }} />
                </CardItem>
                <CardItem style={{ height: 45 }}>
                    <Left>
                        <Button transparent>
                            <Icon name="heart" style={{ color: 'black' }} />
                        </Button>
                        <Button transparent>
                            <Icon name="chatbubbles" style={{ color: 'black' }} />
                        </Button>
                        <Button transparent>
                            <Icon name="send" style={{ color: 'black' }} />
                        </Button>


                    </Left>
                </CardItem>

                <CardItem style={{ height: 20 }}>
                    <Text>{this.props.likes} Likes </Text>
                </CardItem>
                <CardItem>
                    <Body>
                        <Text>
                            <Text style={{ fontWeight: "900" }}>{this.props.usern}
                            </Text>
                            <Text>{"  "}</Text>
                            {this.props.capP}
                        </Text>
                    
                        <Input placeholder='Add a comment...'/>
         

                    </Body>
                </CardItem>
            </Card>


        );
    }
}
export default CardComponent;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});