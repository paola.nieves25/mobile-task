import React, { Component } from "react";
import {
    Text,
    StyleSheet, FlatList
} from "react-native";

import { SearchBar,ListItem } from 'react-native-elements';
import { Icon,Container,Header,View,Button,Input,Item, Thumbnail } from 'native-base'
var djson = require('../../server/data.json');
 const list = djson.search;

 




class SearchTab extends Component {

    constructor() {
        super()
        loading: false,
        this.state = { 
        
            dataSource: [
             djson.search[0],
             djson.search[1],

            ], 
            searchSource :[
                djson.search[0],
                djson.search[1],
            ] ,
         }
    }



    static navigationOptions = {

        tabBarIcon: ({ tintColor }) => (
            <Icon name="search" style={{ color: tintColor }} />
        )
    }

  


    renderItem = ({ item }) => {
      
        return (
            <View>
                 <ListItem
                    onPress ={() => {this.props.navigation.navigate('ProfileTab', {Id: item.userId})}} 
                
                    title={item.username}
                    subtitle={item.full_name}
                    
                    leftAvatar={{ source: { uri: item.profile_picture } }}
             
                           />
                        
            </View>
            
        )

        
    }




    searchFilterFunction = text => {    
        
      
        const newData = this.state.dataSource.filter(list => {      
          const itemData = list.username.toUpperCase();
          const textData = text.toUpperCase();
            
           return itemData.indexOf(textData) > -1;    
        });    
        text: text,
        this.setState({ searchSource: newData });  
       
      };
    


      renderHeader = () => {
        return (
            <View style={{ backgroundColor: 'white' ,  marginTop: 25}}>
            <SearchBar        
            placeholder="Search"        
            lightTheme        
            round
            onChangeText={text => this.searchFilterFunction(text)}
            value={this.state.text}
            autoCorrect={false}    
      

        />
        </View>
        );
    };


    render() {
      
       
        return (
          
            <View >
            <FlatList
             data={this.state.searchSource}    
             renderItem={this.renderItem}    
             keyExtractor={list=> list.username}  
             ListHeaderComponent={this.renderHeader}    
            />     
         </View>
           
     
        );
    }
}
export default SearchTab;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});