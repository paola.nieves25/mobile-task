import React, { Component } from "react";
import {
    Text,
    
    StyleSheet
} from "react-native";

import { Container,Content, Icon,Header,Item,Right, Body,Input,Button} from 'native-base'

import CardComponent from '../CardComponent'

var djson = require('../../server/data.json');

const photo = djson.posts[1].user.profile_picture;
const photo1 = djson.posts[0].user.profile_picture;

const username = djson.posts[1].user.username;

const username1 = djson.posts[0].user.username;

const date = djson.posts[1].created_time;
const date1 = djson.posts[0].created_time;

const  caption =  djson.posts[1].caption;
const  caption1 =  djson.posts[0].caption;

 
import Scon from 'react-native-vector-icons/FontAwesome';

class HomeTab extends Component {

        
    
    constructor(props){
        super(props)
        

        this.state = {
         
           
        };

    }




    static navigationOptions = {


        tabBarIcon: ({ tintColor }) => (
            <Icon name="home" style={{ color: tintColor }} />
        )
    }





    

    render() {
        return (
       <Container style={styles.container}>
           <Header  style={{ backgroundColor: 'white' ,  marginTop: 25}}>
                   <Body><Text style={{ fontSize: 20 , right: -120}}>Instagram</Text></Body>
                    <Right><Scon name="inbox" size={30}  /></Right>
                </Header>
                <Content>
                    <CardComponent imageSource="1"
                     likes="101" 
                    imageP= {photo}
                    usern={username} 
                    dateC={date}
                    capP={caption} />
                    <CardComponent imageSource="2" 
                    likes="201" 
                    imageP= {photo1} 
                    usern={username1} 
                    dateC={date1}
                    capP={caption1} />
             
                </Content>
            </Container>
        );
    }
}
export default HomeTab;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    }
});