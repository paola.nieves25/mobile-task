import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions
} from "react-native";

import { Icon,Container,Header,Left,Body,Right,Button, Content} from 'native-base'
import FeedComponent from "../FeedComponent.js";


var djson = require('../../server/data.json');
const photo = djson.posts[1].user.profile_picture;
const photo1 = djson.posts[0].user.profile_picture;

const username = djson.posts[1].user.username;

const username1 = djson.posts[0].user.username;

const date = djson.posts[1].created_time;
const date1 = djson.posts[0].created_time;

const  caption =  djson.posts[1].caption;
const  caption1 =  djson.posts[0].caption;

const media= djson.posts[1].user.counts.media;
const media1= djson.posts[0].user.counts.media;

const fby=djson.posts[1].user.counts.followed_by;

const fby1=djson.posts[0].user.counts.followed_by;

const follow= djson.posts[1].user.counts.follows;

const follow1= djson.posts[0].user.counts.follows;
   
const fullname= djson.posts[1].user.full_name;
const fullname1= djson.posts[0].user.full_name;

const bio= djson.posts[1].user.bio;
const bio1= djson.posts[0].user.bio;

const web= djson.posts[1].user.website;
const web1= djson.posts[0].user.website;

class ProfileTab extends Component {
        
    
    constructor(props){
        super(props)
        

        this.state = {
            id: this.props.navigation.state.params.Id,
         
            
            
           
        };

    }


 

    static navigationOptions = {

        tabBarIcon: ({ tintColor }) => (
            <Icon name="person" style={{ color: tintColor }} />
        )
    }

    renderMessage = () => {
       


        if(this.state.id === 2){
            return(
                <Container>
            
                <FeedComponent
                  imageP= {photo}
                  usern={username} 
                  mediaU={media}
                  fbU={fby}
                  follow={follow}
                  name={fullname}
                  bio={bio}
                  web={web}
                
                />
         
                  </Container>

            );
          
        }  
        else{
            return(
                <Container>
            
                <FeedComponent
                  imageP= {photo1}
                  usern={username1} 
                  mediaU={media1}
                  fbU={fby1}
                  follow={follow1}
                  name={fullname1}
                  bio={bio1}
                  web={web1}
                
                />
         
                  </Container>
            );
        }
        
    }
 


    render() {
       
     
        return (
        <Container>
            
            {this.renderMessage()}
          
          

         </Container>
        );
    }
}
export default ProfileTab;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});