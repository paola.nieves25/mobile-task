import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image,
    Dimensions
} from "react-native";

import { Icon,Container,Header,Left,Body,Right,Button, Content} from 'native-base'
import CardComponent from "./CardComponent";


var djson = require('../server/data.json');
const photo = djson.posts[1].user.profile_picture;
const photo1 = djson.posts[0].user.profile_picture;

const username = djson.posts[1].user.username;

const username1 = djson.posts[0].user.username;

const date = djson.posts[1].created_time;
const date1 = djson.posts[0].created_time;

const  caption =  djson.posts[1].caption;
const  caption1 =  djson.posts[0].caption;
var images = [
    djson.posts[1].user.feed[0] ,
    djson.posts[1].user.feed[1] ,
    djson.posts[1].user.feed[2] ,
    djson.posts[1].user.feed[3] ,
    djson.posts[1].user.feed[4] ,
    djson.posts[1].user.feed[5] ,
    djson.posts[1].user.feed[6] ,
    djson.posts[1].user.feed[7] ,
    djson.posts[1].user.feed[8] ,
    djson.posts[1].user.feed[9] ,
    djson.posts[1].user.feed[10] ,
    djson.posts[1].user.feed[11] ,
  
]


var { height, width } = Dimensions.get('window');



class FeedComponent extends Component {

    static navigationOptions = {

        tabBarIcon: ({ tintColor }) => (
            <Icon name="person" style={{ color: tintColor }} />
        )
    }



    constructor(props) {
        super(props)

        this.state = {
            activeIndex: 0
        }
    }


    segmentClicked(index) {
        this.setState({
            activeIndex: index
        })
    }
    checkActive = (index) => {
        if (this.state.activeIndex !== index) {
            return (
                { color: 'grey' }
            )
        }
        else {
            return (
                {}
            )
        }

    }

    renderSectionOne = ()=>{
        return images.map((image, index) => {
            return (
                <View key={index} style={[{ width: (width) / 3 }, { height: (width) / 3 }, { marginBottom: 2 }, index % 3 !== 0 ? { paddingLeft: 2 } : { paddingLeft: 0 }]}>
                    <Image style={{
                        flex: 1,
                        alignSelf: 'stretch',
                        width: undefined,
                        height: undefined,
                
                    }}
                    source={{uri:image}}>
                    </Image>

                </View>
                
            )
        })

    }

    






    renderSection = ()=>{

        if (this.state.activeIndex === 0) {

            return (
                <View style={{ flexDirection: 'row',flexWrap: 'wrap'}}>
                   {this.renderSectionOne()}
                </View>

            )
        }
        else if(this.state.activeIndex ==1){
            return(
                <View>
                <CardComponent imageSource="1"
                     likes="101" 
                    imageP= {this.props.imageP}
                    usern={this.props.usern} 
                    dateC={date}
                    capP={caption} />
            </View>

            )
        }

    }




    render() {
       
 
  
        return (
         <Container style={{flex:1,backgroundColor: 'white'}} >
              <Header style={{ backgroundColor: 'white',  marginTop: 25,marginBottom: 10,}}>
                  
                    <Body><Text style={{ fontSize: 20 , right: -130}}>{this.props.usern}</Text></Body>
                    <Right><Icon style={{ paddingRight: 10 }} name="settings" /></Right>
                </Header>
               <Content>
                     <View>
                      <View style={{flexDirection: 'row'}}>
                      <View>
                          <Image style={{ width:75,height:75,borderRadius:37.5}}
                             source={{ uri: this.props.imageP }}/>
                      </View>
                      
                      <View style={{ flex: 3 }}>
                      <View style={{flexDirection: 'row', justifyContent: 'space-around',alignItems: 'flex-end'}}>
                           <View style={{ alignItems: 'center' }}>
                                <Text>{this.props.mediaU}</Text>
                               <Text style={{ fontSize: 10, color: 'grey' }}>Posts</Text>
                                </View>
                                     <View style={{ alignItems: 'center' }}>
                                       <Text>{this.props.fbU}</Text>
                                      <Text style={{ fontSize: 10, color: 'grey' }}>Followers</Text>
                                     </View>
                            <View style={{ alignItems: 'center' }}>
                           <Text>{this.props.follow}</Text>
                           <Text style={{ fontSize: 10, color: 'grey' }}>Following</Text>
                            </View>
                        </View>
                        <View style={{flexDirection:'row'}}>
                        <Button bordered dark style={{ flex: 3, marginLeft: 10, justifyContent: 'center', height: 30 }}><Text>Edit Profile</Text>
                        </Button>
                     

                        </View>
                     </View>
                   </View>    
                   <View style={{ paddingBottom: 10 }}>
                            <View style={{ paddingHorizontal: 10 }}>
                                <Text style={{ fontWeight: 'bold' }}>{this.props.name}</Text>
                                <Text>{this.props.bio}</Text>
                                <Text>{this.props.web}</Text>
                             </View>
                     </View>

                     <View>
                         <View style={{ flexDirection: 'row', justifyContent: 'space-around', borderTopWidth: 1, borderTopColor: '#eae5e5' }} >

                           <Button onPress={() => this.segmentClicked(0)} transparent active={this.state.activeIndex == 0}>
                             <Icon name="apps" style={[this.state.activeIndex == 0 ? {} : { color: 'grey' }]} ></Icon>
                            </Button>
                           
                           <Button onPress={() => this.segmentClicked(1)} transparent active={this.state.activeIndex == 1}>
                           <Icon name="list" style={[{ fontSize: 32 }, this.state.activeIndex == 1 ? {} : { color: 'grey' }]}></Icon>
                           </Button>

                        <Button onPress={() => this.segmentClicked(2)} transparent active={this.state.activeIndex == 2}>
                           <Icon name="bookmark" style={this.state.activeIndex == 2 ? {} : { color: 'grey' }}></Icon>
                          </Button>
                          
                          
                          <Button onPress={() => this.segmentClicked(3)} transparent last active={this.state.activeIndex == 3}>
                         <Icon name="people" style={[{ fontSize: 32 }, this.state.activeIndex == 3 ? {} : { color: 'grey' }]}></Icon>
                          </Button>
                     </View>

                     {this.renderSection()}
                     </View>


                </View>

               </Content>




         </Container>
        );
    }
}
export default FeedComponent;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});