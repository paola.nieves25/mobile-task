This project was bootstrapped with [Create React Native App](https://github.com/react-community/create-react-native-app).

Below you'll find information about performing common tasks. 


## How to run the code


If Yarn was installed when the project was initialized, then dependencies will have been installed via Yarn, and you should probably use it to run these commands as well. Unlike dependency installation, command running syntax is identical for Yarn and NPM at the time of this writing.



### Installing packages and dependencies

```sh
# install dependencies listed in package.json
$ npm install

```



### `npm start`

Runs your app in development mode.

Open it in the [Expo app](https://expo.io) on your phone to view it. It will reload if you save edits to your files, and you will see build errors and logs in the terminal.




## Architecture

This project was design with a client/server model architecture.There's a local json is the server-side of the client/server use case, 
with a client application(React Native app) that access to the static json schema.

![alt text](./Client-Server.png "Description goes here")




## If I had more time, what would I like to improve?

I would've liked to fix bugs and add more features to the application, like add comments or like a post.
