import { createStackNavigator, createAppContainer } from 'react-navigation';
import MainScreen from './Components/MainScreen';

/*export default class App extends Component {
  render() {
    return (
      <AppNavigator />
    );
  }
}*/

const AppNavigator = createStackNavigator({

  Main: {
    screen: MainScreen, navigationOptions: {
      header: null
}
  }

});

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;




